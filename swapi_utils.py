import requests

def get_planets_with_arid_climate():
    url = "https://swapi.dev/api/planets/"
    params = {"climate": "arid"}
    response = requests.get(url, params=params)
    planets = response.json()
    return planets

def get_wookies_in_sixth_movie():
    url = "https://swapi.dev/api/people/"
    params = {"search": "Wookiee", "films": "3"}  # Tercera película es la sexta de la saga
    response = requests.get(url, params=params)
    wookies = response.json()
    return wookies

def get_largest_starship():
    url = "https://swapi.dev/api/starships/"
    response = requests.get(url)
    starships = response.json()["results"]
    largest_starship = max(starships, key=lambda x: int(x["length"]))
    return largest_starship
