from swapi_utils import get_planets_with_arid_climate, get_wookies_in_sixth_movie, get_largest_starship

# Pregunta a)
planets_with_arid_climate = get_planets_with_arid_climate()
count_planets_with_arid_climate = len(planets_with_arid_climate["results"])
print(f"a) En {count_planets_with_arid_climate} planetas el clima es árido.")

# Pregunta b)
wookies_in_sixth_movie = get_wookies_in_sixth_movie()
count_wookies_in_sixth_movie = len(wookies_in_sixth_movie["results"])
print(f"b) Aparecen {count_wookies_in_sixth_movie} Wookies en la sexta película.")

# Pregunta c)
largest_starship = get_largest_starship()
name_of_largest_starship = largest_starship["name"]
print(f"c) La aeronave más grande en toda la saga es: {name_of_largest_starship}")
